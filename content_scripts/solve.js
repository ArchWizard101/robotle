(function () {
  /**
   * Check and set a global guard variable.
   * If this content script is injected into the same page again,
   * it will do nothing next time.
   */
  if (window.hasRun) {
    return;
  }
  window.hasRun = true;

  // Debug console message
  console.log("Robotle> Ready to rock and roll!")

  /**
   * Listen for the solve message from the popup
   * If we get the solve message, pull the Wordle solution from
   * LocalStorage and pass it back to the popup script 
   */
  browser.runtime.onMessage.addListener((message, sender, callback) => {
    if (message.command === "solve") {
      var answer = JSON.parse(window.localStorage.getItem("nyt-wordle-state")).solution
      callback({ solution: answer.toUpperCase() })
    }
  });
})();
