/**
 * Listen for clicks on the buttons, and send the appropriate message to
 * the content script in the page.
 */
function listenForClicks() {
  document.addEventListener("click", (e) => {

    /**
     * Get the active tab,
     * then call the start solving function
     */
    if (e.target.classList.contains("user-button")) {
      browser.tabs.query({ active: true, currentWindow: true })
        .then(startSolving())
    }
  });
}


// Show Robotle's starting messages and the Yes Please button on a 600 ms interval
var htmlElements = document.getElementsByClassName("hidden");
for (var i = 0; i < htmlElements.length; i++) {
  console.log(htmlElements[i])
  setTimeout(() => {
    var elems = document.getElementsByClassName("hidden");
    elems[0].classList.add("fade-in")
    elems[0].classList.remove("hidden")
  },
    600 * i)
}


// Display the solution
function solve(message) {
  robotle = document.getElementById("robotle")
  robotle.src = "../assets/idle.gif"
  messages.insertAdjacentHTML('beforeend', '<p class="robotle-message fade-in">I think the answer is ' + message.solution + '</p>');
};

// Display a failure message if Robotle can't solve the Wordle for some reason
function failure() {
  robotle = document.getElementById("robotle")
  robotle.src = "../assets/idle.gif"
  messages.insertAdjacentHTML('beforeend', '<p class="robotle-message fade-in"> I\'m sorry! I couldn\'t solve this Wordle! ):</p>');
};

function startSolving() {
  // Hide the Yes Please button
  userButton = document.getElementsByClassName("user-button")[0]
  userButton.classList.add("hidden")
  // Replace the existing messages with Hmm..
  messages = document.getElementsByClassName("robotle-messages")[0]
  messages.innerHTML = '<p class="robotle-message fade-in">Hmm...</p>'
  // Play Robotle's thinking animation
  robotle = document.getElementById("robotle")
  robotle.src = "../assets/thinking.gif"

  // Show the "Let me think" message after half a second
  setTimeout(() => {
    messages = document.getElementsByClassName("robotle-messages")[0]
    messages.insertAdjacentHTML('beforeend', '<p class="robotle-message fade-in">Let me think...</p>');
  }, 500)

  // Wait five seconds, then message the content script to extract the wordle answer, 
  // calling solve with the solution, or calling failure with an error
  setTimeout(() => {
    browser.tabs.query({ active: true, currentWindow: true })
      .then(tabs => {
        browser.tabs.sendMessage(tabs[0].id, { command: "solve" }).then(solve, failure);
      })
  }, 5000)

}

// Execute the content script
browser.tabs.executeScript({ file: "/content_scripts/solve.js" }).then(listenForClicks)