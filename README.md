# Robotle

This is a little browser extension that simply reads the local storage of the NYT wordle site and displays the answer through an anthropomorphic robot.
The mechanism for cheating the wordle is very simple, and this is a much more complicated solution than [others](https://www.reviewgeek.com/108165/never-lose-at-wordle-again-with-our-simple-cheat-tool/), but I wanted to practice writing browser extensions for Firefox.

## Installation:

1. Install [Firefox](https://www.mozilla.org/en-US/firefox/new/)

2. Clone this repository

3. Go to `about:addons`

4. Click the gear icon and select "Debug addons"

5. Click "Load temporary addons"

6. Select the `manifest.json` file in the cloned repository

## Usage:

1. Navigate to the latest [Wordle](https://www.nytimes.com/games/wordle/index.html)

2. Click on the Robotle extension

3. Follow Robotle's instructions

#### Acknowledgements:

- The beastify example browser extension from Mozilla
- Lots of StackOverflow
- Chat message style from <https://www.w3schools.com/howto/howto_css_chat.asp>
- The Robotle art from <https://opengameart.org/content/robot2-sprite-sheet>